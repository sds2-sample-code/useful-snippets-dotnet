// sample code - not production ready

// gather all member details
var details = job.GetDrawingHandles(TableWithDrawings.Detail);
List<string> detail_names = new List<string>();
foreach (DrawingHandle kvp in details)
{
    SDS2.Detail.Drawing d = SDS2.Detail.Drawing.Get(kvp);
    detail_names.Add(d.Name);
}

// gather all submaterial details
var subm_names = new List<string>();
var subm = job.GetDrawingHandles(TableWithDrawings.Submaterial);
foreach (DrawingHandle kvp in subm)
{
    SDS2.Detail.Drawing d = SDS2.Detail.Drawing.Get(kvp);
    subm_names.Add(d.Name);
}

// filter member and submaterial details to only those on detail sheets
Console.WriteLine("Find all drawings referencing each drawing piecemark");

foreach (var n in detail_names)
{
    foreach (var kvp in SDS2.Detail.Drawing.Find(SDS2.Detail.PiecemarkType.Major, n))
    {
        if (kvp.DrawingType == TableWithDrawings.DetailSheet)
        {
            Console.WriteLine("Detail {0} is on sheet {1} type of drawing {2}", n, kvp.Name, kvp.DrawingType);
        }
    }
}

foreach (var n in subm_names)
{
    foreach (var kvp in SDS2.Detail.Drawing.Find(SDS2.Detail.PiecemarkType.Minor, n))
    {
        if (kvp.DrawingType == TableWithDrawings.DetailSheet)
        {
            Console.WriteLine("Detail {0} is on sheet {1} type of drawing {2}", n, kvp.Name, kvp.DrawingType);
        }
    }
}
